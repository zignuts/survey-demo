/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  'POST /survey': 'SurveyController.create', //checked
  'DELETE /survey/:id': 'SurveyController.delete', //checked
  'GET /survey/:id': 'SurveyController.getById', //checked

  'POST /survey/answer': 'AnswerController.create', //checked
};
