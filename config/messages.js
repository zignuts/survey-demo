module.exports.messages = {
  SurveyNotFound: 'Survey not found',
  SurveyCreated: 'Survey created successfully',
  SurveyDeleted: 'Survey deleted successfully',
  SurveyDetails: 'Survey details',

  QuestionNotFound: 'Question not found in survey',
  QuestionCreated: 'Question created successfully',
  QuestionDeleted: 'Question deleted successfully',
  QuestionDetails: 'Question details',

  AnswerNotFound: 'Answer not found',
  AnswerCreated: 'Answer created successfully',
  AnswerDeleted: 'Answer deleted successfully',
  AnswerDetails: 'Answer details',
};
