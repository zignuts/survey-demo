# Survey Demo Project

## Summary

This is a demo project created for an assignment. The project has REST APIs for creating and managing the survey. It contains mainly the following features.

- Create a survey 
- Taking a survey
- Getting results of a survey
- For simplicity, the survey questions are of type YES/NO and a survey can consist of maximum 3 questions 

## Technology Stack

The project code is developed using the following technology stack and libraries.

- Node.js
- Sailsjs Framework
- Sails Disk - For data storage, it uses a persistant low-profile storage that comes default with the framework.
- ValidatorJs - For validating the request data 

## Important Files 

- config/routes.js - It has all the API endpoint routes and mapping to respective controller
- config/constants.js - It has the constants that are used across the project
- config/messages.js - It has all the response messages used in the API responses
- api/controllers - Has all the controllers that are mapped with the respective routes
- api/models - It has all the models that represents the database access layer, validations and any pre/post processing logic

## Steps to Setup - Using Docker

- Make sure Docker is installed. Kindly refer to the official doc for installation steps. [Install Docker](https://docs.docker.com/get-docker/)
- Clone this project and cd to the project directory
- Build the project using `docker build -t survey-demo .`
- Run the project using `docker run -dp 1337:1337 --rm --name survey-demo survey-demo`
- Check the logs of the project using `docker logs -f survey-demo`

## Steps to Setup - Without Docker

- Make sure Node and NPM are installed. Recommended to use latest or recent versions of Node. 
- Clone this project and cd to the project directory
- Run `npm install` or `yarn install` to install all the dependencies
- Run `sails lift` to start backend server

## Testing the APIs

- Download and install Postman [Install Postman](https://www.postman.com/downloads/)
- Import a postman collection in your postman app - [Import Collection](https://www.getpostman.com/collections/0745ffd1e43d6813f1ca)

### Usage

- Open postman application
- Import a postman collection from this link - https://www.getpostman.com/collections/0745ffd1e43d6813f1ca
- Locate `create survey ...` API
- Fill questions in `body` tab and hit send
- Locate `get survey details` API
- Provide survey id in `params` tab and Hit send and observe current status of survey
- Locate `Add answer ...` API
- Provide survey id and question id in `params` tab and Hit send
- Locate and send `get survey details` API
- to delete survey Locate `delete survey` API, provide survey id and Hit send

### Reference Links

- [Sails framework documentation](https://sailsjs.com/get-started)
- [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
- [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
- [Community support options](https://sailsjs.com/support)
- [Professional / enterprise options](https://sailsjs.com/enterprise)
