/**
 * Survey.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      description: 'name of survey',
    },
    questions: {
      collection: 'Question',
      via: 'surveyId',
      description: 'populated array of questions',
    },
  },

  /**
   * @function validateCreateSurvey
   * @summary Validate the data before creation
   * @file Survey.js
   * @author Abhi P. (Zignuts Technolabs)
   */
  validateCreateSurvey: async (data) => {
    let rules = {
      name: 'required|string|max:200',
      questions: 'required|array|max:3',
      'questions.*.questionText': 'required|string|max:500',
    };

    let validation = new sails.config.constants.Validator(data, rules);
    let result = {};
    if (validation.fails()) {
      result['hasError'] = true;
      result['errors'] = validation.errors.all();
      return result;
    }

    result['hasError'] = false;
    return result;
  },
};
