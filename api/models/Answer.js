/**
 * Answer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const messages = sails.config.messages;

module.exports = {
  attributes: {
    questionId: {
      model: 'Question',
      required: true,
    },
    surveyId: {
      model: 'Question',
      required: true,
    },
    answerFlag: {
      type: 'boolean',
      required: true,
      description: 'contains real answer yes/no true/false',
    },
  },

  /**
   * @function validateCreateAnswer
   * @summary Validate the data before inserting in the database
   * @file Answer.js
   * @author Abhi P. (Zignuts Technolabs)
   */
  validateCreateAnswer: async (data) => {
    let rules = {
      surveyId: 'required|numeric',
      questionId: 'required|numeric',
      answerFlag: 'required|boolean',
    };

    let validation = new sails.config.constants.Validator(data, rules);
    let result = {};
    if (validation.fails()) {
      result['hasError'] = true;
      result['errors'] = validation.errors.all();
      return result;
    }

    const question = await Question.findOne({
      id: data.questionId,
      surveyId: data.surveyId,
    });

    if (!question) {
      result['hasError'] = true;
      result['errors'] = messages.QuestionNotFound;
      return result;
    }
    result['hasError'] = false;
    return result;
  },

  /**
   * @function afterCreate
   * @summary This is a default hook that will be called after every create statement, updates the count of responses
   * @file Answer.js
   * @author Abhi P. (Zignuts Technolabs)
   */
  afterCreate: async (answer, done) => {
    try {
      let question = await Question.findOne({
        id: answer.questionId,
        surveyId: answer.surveyId,
      });
      let dataToUpdate = {};
      if (answer.answerFlag) {
        dataToUpdate.totalYes = question.totalYes + 1;
      } else {
        dataToUpdate.totalNo = question.totalNo + 1;
      }
      await Question.updateOne({ id: question.id }, dataToUpdate);
      return done();
    } catch (error) {
      return done(error);
    }
  },
};
