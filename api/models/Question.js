/**
 * Question.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    questionText: {
      type: 'string',
      required: true,
      description: 'text of survey question',
    },
    totalYes: {
      type: 'number',
      defaultsTo: 0,
      description: 'Total number of yes/true answers',
    },
    totalNo: {
      type: 'number',
      defaultsTo: 0,
      description: 'Total number of no/false answers',
    },
    surveyId: {
      model: 'Survey',
      required: true,
      description: 'Refers to the Survey model'
    },
  },
};
