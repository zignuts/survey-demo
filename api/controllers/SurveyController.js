/**
 * SurveyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const ResponseCodes = sails.config.constants.ResponseCodes;
const messages = sails.config.messages;
const FileName = 'SurveyController';

module.exports = {
  /**
   * @function create
   * @summary Create a new Survey with initial questions
   * @file SurveyController.js
   * @path POST /survey
   * @author Abhi P. (Zignuts Technolabs)
   */

  create: async (req, res) => {
    sails.log.info(`${FileName} -  create`);

    let data = _.pick(req.body, ['name', 'questions']);

    // Validating the data
    const result = await Survey.validateCreateSurvey(data);

    // In case of any data validation error, return the error response
    if (result['hasError']) {
      return res.status(ResponseCodes.BAD_REQUEST).json({
        status: ResponseCodes.BAD_REQUEST,
        data: '',
        error: result['errors'],
      });
    }

    try {
      // Storing the survey data in the database
      const survey = await Survey.create({ name: data.name }).fetch();
      data.questions = data.questions.map((q) => ({
        ...q,
        surveyId: survey.id,
      }));

      // Storing the questions data for the survey
      survey.questions = await Question.createEach(data.questions).fetch();

      return res.status(ResponseCodes.CREATED).json({
        status: ResponseCodes.CREATED,
        data: survey,
        message: messages.SurveyCreated,
        error: '',
      });
    } catch (error) {
      // Handling error and returning error response in case of any exception
      sails.log.error(`${FileName} -  create error`, error);
      return res.status(ResponseCodes.INTERNAL_SERVER_ERROR).json({
        status: ResponseCodes.INTERNAL_SERVER_ERROR,
        data: '',
        error: error,
      });
    }
  },

  /**
   * @function getById
   * @summary Get survey details with current status / results
   * @file SurveyController.js
   * @path GET /survey/:id
   * @author Abhi P. (Zignuts Technolabs)
   */

  getById: async (req, res) => {
    sails.log.info(`${FileName} -  getById`);

    const surveyId = req.params.id;

    try {
      // Getting the survey by the ID
      const survey = await Survey.findOne({ id: surveyId }).populate(
        'questions'
      );
      
      // If the survey does not exist in the database, return 404
      if (!survey) {
        return res.status(ResponseCodes.NOT_FOUND).json({
          status: ResponseCodes.NOT_FOUND,
          data: survey,
          message: messages.SurveyNotFound,
          error: '',
        });
      }

      return res.status(ResponseCodes.CREATED).json({
        status: ResponseCodes.OK,
        data: survey,
        message: messages.SurveyDetails,
        error: '',
      });
    } catch (error) {
      // Handling error and returning error response in case of any exception
      sails.log.error(`${FileName} -  getById error`, error);
      return res.status(ResponseCodes.INTERNAL_SERVER_ERROR).json({
        status: ResponseCodes.INTERNAL_SERVER_ERROR,
        data: '',
        error: error,
      });
    }
  },

  /**
   * @function delete
   * @summary delete the survey
   * @file SurveyController.js
   * @path DELETE /survey/:id
   * @author Abhi P. (Zignuts Technolabs)
   */

  delete: async (req, res) => {
    sails.log.info(`${FileName} -  delete`);

    const surveyId = req.params.id;

    try {
      // Deleting the survey
      const survey = await Survey.destroyOne({ id: surveyId }).meta({
        cascade: false,
      });

      // If the survey does not exist in the database, return 404
      if (!survey) {
        return res.status(ResponseCodes.NOT_FOUND).json({
          status: ResponseCodes.NOT_FOUND,
          data: survey,
          message: messages.SurveyNotFound,
          error: '',
        });
      }

      return res.status(ResponseCodes.CREATED).json({
        status: ResponseCodes.OK,
        data: survey,
        message: messages.SurveyDeleted,
        error: '',
      });
    } catch (error) {
      // Handling error and returning error response in case of any exception
      sails.log.error(`${FileName} -  delete error`, error);
      return res.status(ResponseCodes.INTERNAL_SERVER_ERROR).json({
        status: ResponseCodes.INTERNAL_SERVER_ERROR,
        data: '',
        error: error,
      });
    }
  },
};
