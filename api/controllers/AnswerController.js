/**
 * AnswerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const ResponseCodes = sails.config.constants.ResponseCodes;
const messages = sails.config.messages;
const FileName = 'AnswerController';

module.exports = {
  /**
   * @function create
   * @summary Submit answer of an question
   * @file AnswerController.js
   * @path POST /answer
   * @author Abhi P. (Zignuts Technolabs)
   */
  create: async (req, res) => {
    sails.log.info(`${FileName} -  create`);

    // Getting the answer type
    let data = _.pick(req.body, ['surveyId', 'questionId', 'answerFlag']);
    //data.surveyId = req.params.sid;
    //data.questionId = req.params.qid;

    // Validating the data
    const result = await Answer.validateCreateAnswer(data);

    // Return error response in case of data validation errors
    if (result['hasError']) {
      return res.status(ResponseCodes.BAD_REQUEST).json({
        status: ResponseCodes.BAD_REQUEST,
        data: '',
        error: result['errors'],
      });
    }

    try {
      // Storing the answer in the database
      const answer = await Answer.create(data).fetch();

      return res.status(ResponseCodes.CREATED).json({
        status: ResponseCodes.CREATED,
        data: answer,
        message: messages.AnswerCreated,
        error: '',
      });
    } catch (error) {
      // Handling error and returning error response in case of any exception
      sails.log.error(`${FileName} -  create error`, error);
      return res.status(ResponseCodes.INTERNAL_SERVER_ERROR).json({
        status: ResponseCodes.INTERNAL_SERVER_ERROR,
        data: '',
        error: error,
      });
    }
  },
};
