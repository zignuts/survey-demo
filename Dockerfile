FROM node:16-alpine
WORKDIR /app
RUN npm install -g sails
EXPOSE 1337
COPY package.json package-lock.json ./
RUN npm install
COPY ./ ./
CMD [ "sails", "lift" ]